/* CSS */
import './scss/main.scss';

const key = require('./key');
const { WeatherData } = require('./api');
const { ShowData } = require('./user'); 
const { LocalStorage } = require('./storage');
const localStorage = new LocalStorage();
const { city } = localStorage.getLocationData();
const weather = new WeatherData(city, key.getToken());
const showData = new ShowData();


async function fetchWeather(){
    const data = await weather.getWeather();
    showData.render(data)
}


document.getElementById('search-btn').addEventListener('click', (e) => {
    const city = document.getElementById('city-input').value;
    weather.changeLocation(city);
    localStorage.setLocationData(city);
    fetchWeather();
    e.preventDefault(); 
})

document.addEventListener('DOMContentLoaded', fetchWeather);


