

export class ShowData {

    constructor(){
        this.location = document.getElementById('city');
        this.temperature = document.getElementById('degrees');
        this.climate = document.getElementById('climate');
        this.country = document.getElementById('country');
        this.humidity = document.getElementById('humidity');
        this.wind = document.getElementById('wind');
        this.feelsLike = document.getElementById('feelsLike');
        this.dayTime = document.getElementById('day-time');
    }

    render(weather){
        this.location.textContent = weather.name;
        this.temperature.innerHTML = Math.trunc(weather.main.temp) + '°' + ' <span class="c">C</span>';
        this.climate.textContent = weather.weather[0]['description'];
        this.country.textContent = ' / ' + weather.sys.country;
        this.humidity.textContent = weather.main.humidity + ' %';
        this.wind.textContent = weather.wind.speed + ' ms';
        this.feelsLike.textContent = Math.trunc(weather.main.feels_like) + ' °C';

        let d = new Date();
        let days = ["Sun", "Mon", "Tue", "Wed", "Thurs", "Fri", "Sat"];
        let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        this.dayTime.innerHTML = days[d.getDay()] + ' ' + d.getDate() + ' ' + months[d.getMonth() + 1];

        if (7 <= d.getHours() && d.getHours() < 20) {
            if (document.body) {
                document.body.className = "day";
            }
        }
        else {
            if (document.body) {
                document.body.className = "night";
            }
        }
    }
}




