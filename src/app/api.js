

export class WeatherData{
    constructor(city, key){

        this.apiKey = key;
        this.city = city;
        this.celsius = '&units=metric';
        this.fahrenheit = '&units=imperial';
    }

    async getWeather(){
        const apiURL = `https://api.openweathermap.org/data/2.5/weather?q=${this.city}${this.celsius}&appid=${this.apiKey}`;
        const cityData = await fetch(apiURL); 
        const data = await cityData.json();

        return data;

    }

    changeLocation(city){
        this.city = city;
    }
}


