
# Description
- Aplicación de clima con Nodejs, POO en JavaScript, webpack y SCSS
- Weather Application using Node Js, Webpack, Javascript OOP and SCSS.

# Requirements
- You need to create an account in https://openweathermap.org/api to get an valid API token for using the weather API.
- Es necesario registrarse en  https://openweathermap.org/api para obtener un Token y poder usar su API del clima.

- After instaling the app go to src/app/key.js and paste your Token in the following function:
- Una vez instalada la aplicación, ir a src/app/key.js y pegar tu Token dentro de la siguiente función:

    <code>
    const getToken = () => {
        return 'Paste your Token here';
    };</code>


# Run application in dev:
    - "npm install" => creates node_modules
    - "npm run dev"

# Create dist folder for production:
    -"npm run build"




# **************************************************************************

# Extra - Configuración básica de Webpack en un proyecto nuevo : 

- Estructura básica del proyecto.

    src -
        - app
            - index.js
            - resto de srcripts que necesitemos
        - index.html

- Instalamos webpack. Desde la raiz del proyecto escribimos en la terminal:

    - npm init --yes  ( para crear el package.json)

    - Ahora vamos a instalar webpack con los diferentes modulos que necesitamos. Escribimos todo junto en terminal: 

    npm i webpack-cli
            style-loader
            css-loader
            webpack-dev-server
            html-webpack-plugin

    - Cuando termine la instalación aparecerá la carpeta node_modules y el package-lock.json

    - Desde la raiz del proyecto creamos un archivo vacio de configuración de webpack: webpack.config.js

    - En webpack.config.js vamos a exportar los modulos que necesitemos y vamos a configurarlos .

    - Despues vamos al package.json y configuramos un comando , por ejemplo "build" para que suba los archivos a producción. Para ello usamos el comando webpack -p.

            "scripts": {
            "build": "webpack -p"
        },

    - si desde la consola hacemos npm run build deberia convertir el código de webpack y general un bundle.js y un index.html en la carpeta dist, como hemos configurado en el webpack.config.js

    - En el index.html creará un enlace al bundle.js automaticamente

    - para no usar continuamente el comando npm run build podemos usar el servidor de webpack, configurando el package.json de la siguiente manera:
         "scripts": {
            "build": "webpack -p",
            "dev": "webpack-dev-server -d"
        },

    - Por ultimo vamos a indicar en el index.js que cargue los estilos css. Para ello creamos dentro de app un index.css y lo llamamos desde el index.js de la siguiente manera:
        require('./index.css');