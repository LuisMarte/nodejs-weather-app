const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');

const path = require('path');

module.exports = {
    // Configuramos los archivos de entrada(index.js) y de salida(carpeta dist - se creará automticamente)
    entry: './src/app/index.js', 
    
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    // Configuración del servidor de desarrollo
    devServer: {
        port: 3000
    },
    // establecemos las reglas de webpack
    module: {
        rules: [
          {
            test: /\.s[ac]ss$/i,
            use: [
              // Creates `style` nodes from JS strings
              'style-loader',
              // Translates CSS into CommonJS
              'css-loader',
              // Compiles Sass to CSS
              'sass-loader',
            ],
          },
        ],
      },
    //Le indicamos donde esta el html que tiene que copiar en dist
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new MiniCSSExtractPlugin({
            //filename: './styles/main.css',
        })
    ]
};